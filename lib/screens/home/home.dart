import 'package:flutter/material.dart';
import 'package:flutter_chat_message_app/constants/colors.dart';
import 'package:flutter_chat_message_app/screens/home/widgets/messages.dart';
import 'package:flutter_chat_message_app/screens/home/widgets/recent_contacts.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimary,
      body: Container(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 25, top: 20),
              child: Text('Chat with\nyour friends',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 26,
              ),),
            ),
            
            // === Class RecentContacts ===
            RecentContacts(),

            // === Class Messages ===
            Messages(),
          ],
        ),
      ),
    );
  }
}