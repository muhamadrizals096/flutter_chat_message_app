import 'package:flutter/material.dart';
import 'package:flutter_chat_message_app/models/user.dart';

class Message {
  User user;
  String lastMessage;
  String lastTime;
  bool isContinue;

  Message(this.user, this.lastMessage, this.lastTime,
      {this.isContinue = false});

  static List<Message> generateHomePageMessage() {
    return [
      Message(
        users[0],
        'Thank you for visiting our website. Hope to see you once again. Good day.',
        '18:32',
      ),
      Message(
        users[1],
        'Thanks for stopping by, we hope to hear from you again!',
        '10:42',
      ),
      Message(
        users[2],
        'We appreciate your business and hope to hear from you soon! Thanks for chatting.',
        '12:45',
      ),
      Message(
        users[3],
        'Glad I could help! Wish you a good day and thanks for calling our chat support.',
        '13:32',
      ),
      Message(
        users[4],
        'If any other questions arise, please feel free to contact us at any time. Thanks so much for calling. Good bye.',
        '14:38',
      ),
      Message(
        users[5],
        'Thanks for using our 24-hour help service, and please feel free to contact us again if you need any further assistance. Goodbye!',
        '15:34',
      ),
    ];
  }

  // message from first users
  static List<Message> generateMessagesFromUser() {
    return [
      Message(
        users[0], 
        'Hey there! What\'s up? is everything going well?', 
        '18.00',
      ),
      Message(
        me, 
        'Nothing.. Just chilling, watch youtube and playing video game haha.. What are u doing there?', 
        '18.01',
      ),
      Message(
        users[0], 
        'Same here.. just playing game for 5 hours, eat n sleep haha..', 
        '18.04',
        isContinue: true,
      ),
      Message(
        users[0], 
        'It\'s hard to be productive man :(', 
        '18.06',
      ),
      Message(
        me, 
        'Yeah.. same position man', 
        '18.08',
      ),
      Message(
        users[0], 
        'Lol..', 
        '18.10',
        isContinue: true,
      ),
      Message(
        users[0], 
        'Are u free right now?', 
        '18.11',
      ),
    ];
  }
}

var users = User.generateUsers();
var me = User(
  0,
  'Rafly',
  'Maulana',
  'assets/images/user0.png',
  Color(0xffffff),
);
